
resource "aws_s3_bucket" "resourcely-er_mS5bYGZECuBZgvjv" {
  bucket = "resourcely-er"

  tags = {
    "application" = "as"
    "resourcely_instance_name" = "resourcely-er_mS5bYGZECuBZgvjv"
  }
}

resource "aws_s3_bucket_public_access_block" "resourcely-er_mS5bYGZECuBZgvjv" {
    bucket = aws_s3_bucket.resourcely-er_mS5bYGZECuBZgvjv.id

    block_public_acls       = true
    block_public_policy     = true
    ignore_public_acls      = true
    restrict_public_buckets = true
}

resource "aws_s3_bucket_ownership_controls" "resourcely-er_mS5bYGZECuBZgvjv" {
  bucket = aws_s3_bucket.resourcely-er_mS5bYGZECuBZgvjv.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_versioning" "resourcely-er_mS5bYGZECuBZgvjv" {
    bucket = aws_s3_bucket.resourcely-er_mS5bYGZECuBZgvjv.id
    versioning_configuration {
        status = "Enabled"
    }
}
